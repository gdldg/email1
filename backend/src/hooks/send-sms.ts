// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const {result} = context;
    console.log('sending sms');
    
    console.log(result['to']);
    const accountSid = 'AC4241ab879c45a567ad4779c1ced9515c';
    const authToken = '509e1233cafb9b81ea30b48d0aac17be';
    const client = require('twilio')(accountSid, authToken);

    client.messages
    .create({
      body: 'TESTING!!',
      from: '+16046706989',
      to: result['to']
    })
    .then((message:any) => console.log(`message sent: ${message.sid}`));
    return context;
  };
}
